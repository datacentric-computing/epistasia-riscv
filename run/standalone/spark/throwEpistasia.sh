#!/bin/bash

# ARRIESGADO IPs
a1=192.168.3.79
a2=192.168.3.80
a3=192.168.3.81
a4=192.168.3.82
a5=192.168.3.83
a6=192.168.3.84
a7=192.168.3.85
a8=192.168.3.86

OUTPATH=$HOME/via-riscv/results/logs

# MASTER CONF
masterip=$a5
anode=5
nodes=1
echo $masterip > $HOME/via-riscv/scripts/spark/master.txt
sed -r 's/(\b[0-9]{1,3}\.){3}[0-9]{1,3}\b'/"$masterip"/ $HOME/via-riscv/scripts/main/sparkmdr_x86.py > $HOME/via-riscv/scripts/main/sparkmdr_x86_out.py

echo "Master is in node"
cat $HOME/via-riscv/scripts/spark/master.txt

echo "Worker is in node"
cat $HOME/via-riscv/scripts/spark/master.txt

# PROCESS FILES
for files in $(echo "5"); do 
	#for cores in $(echo "1 2 4"); do
	for cores in $(echo "4"); do
		partitions=$nodes

                # SKIP IF ALREADY PROCESSED 
		if test -f "$OUTPATH/mdr_st_f${files}_n${nodes}_c${cores}.log"; then
			echo "Skipping mdr_st_f${files}_n${nodes}_c${cores} - already done"
		else	       
		       echo "Waiting for nodes ${lnodes}"
		       sbatch -o $OUTPATH/mdr_st_f${files}_n${nodes}_c${cores}.log -W -p arriesgado-jammy -w arriesgado-$anode -t 08:00:00 jscript_epistasia_cores.sh $cores $files $partitions $nodes $it
		fi
    done
   done
