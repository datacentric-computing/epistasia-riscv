#!/bin/bash

if [[ $# -ne 3 ]] ; then
    echo "Please add the following arguments:"
    echo "-- Number of files"
    echo "-- Number of cores"
    echo "-- Number of nodes"
    exit 0
fi

cores=$1
files=$2
nodes=$3
partitions=$3

episDir=$HOME/via-riscv

# First, we copy the number of workers we want to the workers file
head -$nodes $VIAHOME/main/nodes/workers_lichee_list.txt > $VIAHOME/main/nodes/workers_lichee.txt

# Start singularity and spark
bash $episDir/scripts/spark/start_spark_traces.sh $episDir/scripts/spark/master_lichee.txt $episDir/scripts/spark/workers_lichee.txt $cores
MASTER=$(head -1 $episDir/scripts/spark/master_lichee.txt)

# Copy files to all nodes nodes
for WORKER in $(cat $episDir/scripts/spark/workers.txt); do
  printf "\n--- Copying input data to working node $WORKER\n"
  ssh $WORKER "mkdir -p /tmp/via/input; mkdir -p /tmp/via/output; cp -r $episDir/sdata/input/* /tmp/via/input/" 
  ssh $WORKER "ls /tmp/via/input/"
done

printf "\n--- Copying input data to master node $MASTER\n"
ssh $MASTER "mkdir -p /tmp/via/input; mkdir -p /tmp/via/output; cp -r $episDir/sdata/input/* /tmp/via/input/"
ssh $MASTER "ls /tmp/via/input/"

# Throw script
cd $HOME
ssh $MASTER "source /etc/profile; module load singularity; singularity exec instance://spark python3 $episDir/scripts/main/sparkmdr_x86_out.py -f $files -c $cores -p $partitions -n $nodes"


# Copy history app
printf "\n--- Copying spark-events to directory\n"
ssh $MASTER "mv /tmp/spark-events/* $episDir/results/spark-events/riscv_lichee_app_I${it}_W${nodes}_C${cores}_F${files}" 

# Stop Spark and instance
module load singularity
bash $episDir/scripts/spark/stop_spark.sh $episDir/scripts/spark/master_lichee.txt $episDir/scripts/spark/workers_lichee.txt



