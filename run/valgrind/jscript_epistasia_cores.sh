#!/bin/bash

if [[ $# -ne 5 ]] ; then
    echo "Please add the following arguments:"
    echo "-- Number of files"
    echo "-- Number of cores"
    echo "-- Number of partitions"
    echo "-- Number of nodes"
    echo "-- Iteration number (use 0 as default)"
    exit 0
fi

cores=$1
files=$2
partitions=$3
nodes=$4
it=$5

episDir=$HOME/via-riscv

# Load Singularity
module load singularity

# Start Spark
bash $episDir/scripts/spark/start_spark_valgrind.sh $episDir/scripts/spark/master.txt $episDir/scripts/spark/workers.txt $cores
MASTER=$(head -1 $episDir/scripts/spark/master.txt)

# Copy files to spark nodes
for WORKER in $(cat $episDir/scripts/spark/workers.txt); do
  printf "\n--- Copying input data to working node $WORKER\n"
  ssh $WORKER "mkdir -p /tmp/via/input; mkdir -p /tmp/via/output; cp -r $episDir/sdata/input/* /tmp/via/input/" 
  ssh $WORKER "ls /tmp/via/input/"
done

printf "\n--- Copying input data to master node $MASTER\n"
ssh $MASTER "mkdir -p /tmp/via/input; mkdir -p /tmp/via/output; cp -r $episDir/sdata/input/* /tmp/via/input/"
ssh $MASTER "ls /tmp/via/input/"

# Throw script
cd $HOME
printf "\n--- Running script with valgrind\n"
ssh $MASTER "source /etc/profile; module load singularity; singularity exec instance://spark python3 -m cProfile -o $episDir/results/logs/profile_riscv_f${files}_c${cores}_n${nodes}.pyprof $episDir/scripts/main/sparkmdr_x86_out.py -f $files -c $cores -p $partitions -n $nodes"

# Copy history app
printf "\n--- Copying spark-events to directory\n"
ssh $MASTER "mv /tmp/spark-events/* $episDir/results/spark-events/riscv_app_I${it}_W${nodes}_C${cores}_F${files}" 

# Stop Spark and instance
module load singularity
bash $episDir/scripts/spark/stop_spark.sh $episDir/scripts/spark/workers.txt

echo "*************"
echo "JSCRIPT DONE!"
echo "*************"


