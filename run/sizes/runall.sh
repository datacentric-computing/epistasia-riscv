
echo "MODE 2: multiple file sizes"

for size in $(echo "1 2 5 10 20 50 100"); do
       	bash throwEpistasia.sh 1 $size
       	bash throwEpistasia.sh 2 $size
	bash throwEpistasia.sh 3 $size

	mkdir $VIAHOME/results/s$size
	mv $VIAHOME/results/logs $VIAHOME/results/s$size/
	mv $VIAHOME/results/output $VIAHOME/results/s$size/
	mkdir $VIAHOME/results/logs
	mkdir $VIAHOME/results/output
