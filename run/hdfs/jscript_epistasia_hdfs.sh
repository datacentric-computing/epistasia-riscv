#!/bin/bash

if [[ $# -ne 4 ]] ; then
    echo "Please add the following arguments:"
    echo "-- Number of files"
    echo "-- Number of cores"
    echo "-- Number of partitions"
    echo "-- Number of nodes"
    exit 0
fi

cores=$1
files=$2
partitions=$3
nodes=$4

episDir=$HOME/via-riscv

# Load Singularity
module load singularity

# Start Zookeeper, Spark and HDFS
echo "Running script for starting the servers..."
bash $episDir/scripts/hdsp/start_all.sh $episDir/scripts/hdsp/workers.txt $cores
MASTER=$(head -1 $episDir/scripts/hdsp/master.txt)

# Copy data files to HDFS
echo "Copying data files to HDFS"
singularity exec instance://ihbsp hdfs dfs -mkdir /input
singularity exec instance://ihbsp hdfs dfs -mkdir /output
singularity exec instance://ihbsp hdfs dfs -put $episDir/sdata/input/* /input/

# Throw script
cd $HOME
ssh $MASTER "source /etc/profile; module load singularity; singularity exec instance://spark python3 $episDir/scripts/main/hdspmdr_x86_out.py -f $files -c $cores -p $partitions -n $nodes"

# Stop Spark and instance
module load singularity
bash $episDir/scripts/hdsp/stop_all.sh $episDir/scripts/hdsp/workers.txt

echo "*************"
echo "JSCRIPT DONE!"
echo "*************"


