#!/bin/bash

# ARRIESGADO IPs
a1=192.168.3.79
a2=192.168.3.80
a3=192.168.3.81
a4=192.168.3.82
a5=192.168.3.83
a6=192.168.3.84
a7=192.168.3.85
a8=192.168.3.86

OUTPATH=$HOME/via-riscv/results/logs

if [[ $# -eq 0 ]] ; then
    echo "Please add the number of nodes to use"
    exit 0
fi

nodes=$1

# WORKERS CONF
case "$nodes" in
    1) 
	    echo "Running with 1 node"
	    echo $a6 > $HOME/via-riscv/scripts/hdsp/workers.txt
	    lnodes="5-6"
	    ;;

    2) 
	    echo "Running with 2 nodes"
	    echo $a6 > $HOME/via-riscv/scripts/hdsp/workers.txt
	    echo $a7 >> $HOME/via-riscv/scripts/hdsp/workers.txt
	    lnodes="2-6"
	    ;;

    3) 
	    echo "Running with 3 nodes"
	    echo $a6 > $HOME/via-riscv/scripts/hdsp/workers.txt
	    echo $a7 >> $HOME/via-riscv/scripts/hdsp/workers.txt
	    echo $a8 >> $HOME/via-riscv/scripts/hdsp/workers.txt
	    lnodes="3-6"
	    ;;
esac


# MASTER CONF
masterip=$a5
echo $masterip > $HOME/via-riscv/scripts/hdsp/master.txt
sed -r 's/(\b[0-9]{1,3}\.){3}[0-9]{1,3}\b'/"$masterip"/ $HOME/via-riscv/scripts/main/hdspmdr_x86.py > $HOME/via-riscv/scripts/main/hdspmdr_x86_out.py

echo "Master is"
cat $HOME/via-riscv/scripts/hdsp/master.txt

echo "Workers are:"
cat $HOME/via-riscv/scripts/hdsp/workers.txt

# PROCESS FILES
#for files in $(echo "1 3 5"); do 
for files in $(echo "1"); do #throw just 1 experiment
	for cores in $(echo "3"); do
		partitions=$(($nodes*$cores))

                if [[ "$cores" == 4 ]]; then
			partitions=$(($nodes*2))
	        fi


                if [[ "$cores" == 3 ]]; then
			partitions=$(($nodes*2))
	        fi

                # SKIP IF ALREADY PROCESSED 
		if test -f "$OUTPATH/mdr_f${files}_n${nodes}_c${cores}.log"; then
			echo "Skipping mdr_f${files}_n${nodes}_c${cores} - already done"
		else	       
		       echo "Waiting for nodes ${lnodes}"
		       sbatch -o $OUTPATH/mdr_f${files}_n${nodes}_c${cores}.log -W -p arriesgado-jammy -w arriesgado-[${lnodes}] -t 08:00:00 jscript_epistasia_hdfs.sh $cores $files $partitions $nodes
		fi
     wait
    done
   done
