#!/bin/bash

if [[ $# -ne 4 ]] ; then
    echo "Please add the following arguments:"
    echo "-- Number of files"
    echo "-- Number of cores"
    echo "-- Number of partitions"
    echo "-- Number of nodes"
    exit 0
fi

cores=$1
files=$2
partitions=$3
nodes=$4

episDir=$HOME/via-riscv

# Load Singularity
module load singularity


# Start Spark
bash $episDir/scripts/spark/start_spark_standalone.sh $episDir/scripts/spark/master.txt $cores
MASTER=$(head -1 $episDir/scripts/spark/master.txt)

printf "\n--- Copying input data to master node $MASTER\n"
ssh $MASTER "mkdir -p /tmp/via/input; mkdir -p /tmp/via/output; cp -r $episDir/sdata/input/* /tmp/via/input/"
ssh $MASTER "ls /tmp/via/input/"

# Throw script
cd $HOME
ssh $MASTER "source /etc/profile; module load singularity; singularity exec instance://spark python3 $episDir/scripts/main/sparkmdr_x86_out.py -f $files -c $cores -p $partitions -n $nodes"

# Copy history app
printf "\n--- Copying spark-events to directory\n"
ssh $MASTER "mv /tmp/spark-events/* $episDir/results/spark-events/riscv_st_app_I${it}_W${nodes}_C${cores}_F${files}" 

# Stop Spark and instance
module load singularity
bash $episDir/scripts/spark/stop_spark.sh $episDir/scripts/spark/workers.txt

echo "*************"
echo "JSCRIPT DONE!"
echo "*************"


