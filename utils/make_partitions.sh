#FILE=/home/ggomez/Epistasia/data/input/NuGENE_chr22_repaired.gz
#SUFFIX=chr22_NuGENE
#OUTPATH=/home/ggomez/Epistasia/data/input/split_data

FILE=/home/ggomez/Epistasia/riskv/data/input/chr22_NuGENE01.gz
SUFFIX=chr22_NuGENE
OUTPATH=/home/ggomez/Epistasia/riskv/data/input/partitions

OUTFILE=${FILE%.*}
echo $OUTFILE

mkdir -p $OUTPATH/

gzip -d -k $FILE
split -l 50 -d -a 4 $OUTFILE $OUTPATH/$SUFFIX
gzip $OUTPATH/*

# Create list of partitions
ls $OUTPATH/ > $OUTPATH/listoffiles.txt

