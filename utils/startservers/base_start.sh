# Read workers file
# By default the first worker will be set up as the master and a worker

# Set Master
MASTER=$(head -n 1 $1)

# Create instances
while read -u10 WORKER; do
	echo "Starting instance in node " + WORKER
        # Clean old hdfs nodes
	ssh $WORKER rm -r /tmp/*
	ssh $WORKER mkdir -p /tmp/hdfs/namenode
	ssh $WORKER mkdir -p /tmp/hdfs/datanode
	ssh $WORKER mkdir -p /tmp/spark/logs /tmp/spark/work
        ssh $WORKER singularity instance start --bind /tmp/spark/logs/:/opt/spark/logs,/tmp/spark/work/:/opt/spark/work ibsc_hbsp_mdr.sif ihbsp
done 10< $1

# Format HDFS
ssh $MASTER singularity exec instance://ihbsp hdfs namenode -format

# Init master
ssh $MASTER singularity exec instance://ihbsp spark-daemon.sh start org.apache.spark.deploy.master.Master 1 --host $MASTER --port 7078 --webui-port 8081
ssh $MASTER singularity exec instance://ihbsp hdfs --daemon start namenode
ssh $MASTER singularity exec instance://ihbsp yarn --daemon start resourcemanager

# Init workers
n=1
while read -u10 WORKER; do
        port=$((8081+$n))
        ssh $WORKER singularity exec instance://ihbsp spark-daemon.sh start org.apache.spark.deploy.worker.Worker $n --webui-port $port $MASTER:7078;
	ssh $WORKER singularity exec instance://ihbsp hdfs --daemon start datanode
	ssh $WORKER singularity exec instance://ihbsp yarn --daemon start nodemanager
        n=$(($n+1))
done 10< $1

