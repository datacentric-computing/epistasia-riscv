FILE=/home/ubuntu/epistasis/data/input/NuGENE_chr22_repaired.gz
SUFFIX=chr22_NuGENE
OUTPATH=/home/ubuntu/epistasis/data/input/split_data


OUTFILE=${FILE%.*}
echo $OUTFILE

mkdir -p $OUTPATH/small_partitions

gzip -d -k $FILE
split -l 100 -d $OUTFILE $OUTPATH/small_partitions/$SUFFIX
gzip $OUTPATH/small_partitions/*

# Create list of partitions
ls $OUTPATH/small_partitions/ > $OUTPATH/small_listoffiles.txt

