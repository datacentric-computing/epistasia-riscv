# Create TMP directory
ssh bscdc18 mkdir -p /tmp/zookeeper
ssh bscdc19 mkdir -p /tmp/zookeeper
ssh bscdc20 mkdir -p /tmp/zookeeper

# Create ID Files
echo 1 > /tmp/zookeeper/myid1
echo 2 > /tmp/zookeeper/myid2
echo 3 > /tmp/zookeeper/myid3

# Copy ID Files
scp /tmp/zookeeper/myid1 bscdc18:/tmp/zookeeper/myid
scp /tmp/zookeeper/myid2 bscdc19:/tmp/zookeeper/myid
scp /tmp/zookeeper/myid3 bscdc20:/tmp/zookeeper/myid

# Start Zookeeper
ssh bscdc18 singularity exec instance://ihbsp zkServer.sh start
ssh bscdc19 singularity exec instance://ihbsp zkServer.sh start
ssh bscdc20 singularity exec instance://ihbsp zkServer.sh start


# View Zookeeper status
ssh bscdc18 singularity exec instance://ihbsp zkServer.sh status
ssh bscdc19 singularity exec instance://ihbsp zkServer.sh status
ssh bscdc20 singularity exec instance://ihbsp zkServer.sh status
