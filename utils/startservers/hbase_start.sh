# Start Master
ssh bscdc18 singularity exec instance://ihbsp hbase-daemon.sh start master

# Start RegionServers
ssh bscdc19 singularity exec instance://ihbsp hbase-daemon.sh start regionserver
ssh bscdc20 singularity exec instance://ihbsp hbase-daemon.sh start regionserver

# Start Back up Master
ssh bscdc19 singularity exec instance://ihbsp hbase-daemon.sh start master


