# Read workers file
# By default the first worker will be set up as the master and a worker

# Set Master
MASTER=$(head -n 1 $1)


# Stop master
ssh $MASTER singularity exec instance://ihbsp spark-daemon.sh stop org.apache.spark.deploy.master.Master 1
ssh $MASTER singularity exec instance://ihbsp hbase-daemon.sh stop master
ssh $MASTER singularity exec instance://ihbsp hdfs --daemon stop namenode
ssh $MASTER singularity exec instance://ihbsp yarn --daemon stop resourcemanager

# Stop workers
n=1
while read -u10 WORKER; do
        ssh $WORKER singularity exec instance://ihbsp spark-daemon.sh stop org.apache.spark.deploy.worker.Worker $n
	ssh $WORKER singularity exec instance://ihbsp hbase-daemon.sh stop thrift
	ssh $WORKER singularity exec instance://ihbsp hdfs --daemon stop datanode
	ssh $WORKER singularity exec instance://ihbsp yarn --daemon stop nodemanager
        ssh $WORKER singularity exec instance://ihbsp zkServer.sh stop
	ssh $WORKER singularity exec instance://ihbsp hbase-daemon.sh stop regionserver
	n=$(($n+1))
done 10< $1


while read -u10 WORKER; do
        ssh $WORKER singularity instance stop ihbsp
done 10< $1
