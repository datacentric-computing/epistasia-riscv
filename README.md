# Variant Interaction Analysis for RISC-V

#1 - Build the singularity image
sudo singularity build singularity/iriscv_hbsp_mdr.sif singularity/build/defs/iriscv_hbsp_mdr.def

#2 - Run your experiment
cd run/traces
./runall.sh # this script will run several experiments with diferent workers, files and nodes.  


## Details of the directories
# /results
Directory to save the results of the experiments. You can find logs of already performed experiments.

# /run
Running scripts for different experiments.
- regular: no hdfs and no extra information
- traces: get spark-history-server information
- standalone: no spark
- hdfs: add hdfs
- sizes: test with different file sizes
- valgrind: get valgrind information
- lichee: traces run without slurm

# /scripts
Main scripts that are call during the running. 

# /sdata
Data folder with prepared chunks for testing

# /singularity
Definition files, configuration and .sif files for using singularity

# /utils
Other secondary scritps used for running the application





