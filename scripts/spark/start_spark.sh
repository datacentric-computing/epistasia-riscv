# Read workers and master file
# Pease make sure you have ssh connection to all the workers 
# Please make sure that the all the workers have the spark.sif in a folder named spark in their root user directory or change the path in the code below

# Set Master
MASTER=$(cat $HOME/via-riscv/scripts/spark/master.txt)
IMAGE_PATH=$HOME/via-riscv/singularity/iriscv_hbsp_mdr.sif

CORES=$2


echo -e "\n------------------"
echo "Running STARTSPARK.py"
echo -e "------------------\n"

echo "-- Creating spark instances"
# Create spark instances
while read -u10 WORKER; do 
	echo "-- Creating tmp directories and throwing spark instance in $WORKER"
	ssh $WORKER rm -r /tmp/spark/logs /tmp/spark/work
	ssh $WORKER mkdir -p /tmp/spark/logs /tmp/spark/work
        ssh $WORKER "source /etc/profile; module load singularity; singularity instance start --bind /tmp/spark/logs/:/opt/spark/logs,/tmp/spark/work/:/opt/spark/work $IMAGE_PATH spark"
done 10< $1

# Create spark instances
echo "-- Creating tmp directories and starting spark instance in $MASTER"
ssh $MASTER rm -r /tmp/spark/logs /tmp/spark/work
ssh $MASTER mkdir -p /tmp/spark/logs /tmp/spark/work
ssh $MASTER "source /etc/profile; module load singularity; singularity instance start --bind /tmp/spark/logs/:/opt/spark/logs,/tmp/spark/work/:/opt/spark/work $IMAGE_PATH spark"


# Init master
echo "-- Starting MASTER"
ssh $MASTER "source /etc/profile; module load singularity; singularity exec instance://spark spark-daemon.sh start org.apache.spark.deploy.master.Master 1 --host $MASTER --port 7078 --webui-port 8081"

# Init workers
n=1
while read -u10 WORKER; do 
        port=$((8081+$n))
	echo "-- Starting WORKER $WORKER"
	ssh $WORKER "source /etc/profile; module load singularity; singularity exec instance://spark spark-daemon.sh start org.apache.spark.deploy.worker.Worker $n $MASTER:7078 --webui-port $port --cores $CORES;"
        n=$(($n+1))
done 10< $1

echo ""
echo -e "\n------------------"
echo "STARTSPARK.py done!"
echo -e "------------------\n"


