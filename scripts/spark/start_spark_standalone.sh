# Read workers and master file
# Pease make sure you have ssh connection to all the workers 
# Please make sure that the all the workers have the spark.sif in a folder named spark in their root user directory or change the path in the code below

# Set Master
MASTER=$(cat $1)
IMAGE_PATH=$HOME/via-riscv/singularity/iriscv_hbsp_mdr.sif

CORES=$2


echo -e "\n------------------------------"
echo "Running STARTSPARK_STANDALONE.py"
echo -e "--------------------------------\n"

# Create spark instances
echo "-- Creating tmp directories and starting spark instance in $MASTER"
ssh $MASTER rm -r /tmp/spark/logs /tmp/spark/work /tmp/spark-events
ssh $MASTER mkdir -p /tmp/spark/logs /tmp/spark/work /tmp/spark-events
ssh $MASTER "source /etc/profile; module load singularity; singularity instance start --bind /tmp/spark/logs/:/opt/spark/logs,/tmp/spark/work/:/opt/spark/work,/home/ggomez/via-riscv/singularity/build/confcluster/conf_spark:/opt/spark/conf $IMAGE_PATH spark"

# Init master
echo "-- Starting MASTER"
ssh $MASTER "source /etc/profile; module load singularity; singularity exec instance://spark spark-daemon.sh start org.apache.spark.deploy.master.Master 1 --host $MASTER --port 7078 --webui-port 8081"

# Init workers
ssh $MASTER "source /etc/profile; module load singularity; singularity exec instance://spark spark-daemon.sh start org.apache.spark.deploy.worker.Worker 2 $MASTER:7078 --webui-port 8082 --cores $CORES;"

echo ""
echo -e "\n------------------"
echo "STARTSPARK.py done!"
echo -e "------------------\n"


