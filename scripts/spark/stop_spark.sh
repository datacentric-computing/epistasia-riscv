# Read workers file
# By deafult the first worker will be set up as the master and a worker

# Set Master
MASTER=$(cat $HOME/via-riscv/scripts/spark/master.txt)

echo -e "\n-------------------"
echo "Running STOPSPARK.py"
echo -e "-------------------\n"



# Stop master
echo "-- Stopping MASTER $MASTER"
ssh $MASTER "source /etc/profile; module load singularity; singularity exec instance://spark spark-daemon.sh stop org.apache.spark.deploy.master.Master 1"
ssh $MASTER "source /etc/profile; module load singularity; singularity instance stop spark"

# Stop workers
n=1
while read -u10 WORKER; do 
	echo "Stopping WORKER $WORKER"
	ssh $WORKER "source /etc/profile; module load singularity; singularity exec instance://spark spark-daemon.sh stop org.apache.spark.deploy.worker.Worker $n"
        ssh $WORKER "source /etc/profile; module load singularity; singularity instance stop spark"
done 10< $1


echo -e "\n-------------------"
echo "STOPSPARK.py done!"
echo -e "-------------------\n"






