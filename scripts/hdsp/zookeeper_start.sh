# Set Master
MASTER=$(head -n 1 $1)
WORKERS=$2

# Starting Zookeeper in MASTER
n=1
echo "-- Configuring Zookeeper in MASTER $MASTER"

# Making directories and copying configuration files
ssh $MASTER "rm -r /tmp/zookeeper /tmp/zk_log; mkdir -p /tmp/zookeeper /tmp/zk_logs"
ssh $MASTER cp $HOME/via-riscv/singularity/build/confcluster/conf_zk/conf/* /tmp/zk_conf/ # Copy configuration files to conf directory
echo $n > /tmp/zookeeper/myid_sample # Create ID file
scp /tmp/zookeeper/myid_sample $MASTER:/tmp/zookeeper/myid # Copy to server

# Edit configuration in MASTER node
ssh $MASTER "echo server.1=0.0.0.0:2888:3888 >> /tmp/zk_conf/zoo.cfg"
n=2
while read -u10 WK; do
	ssh $MASTER "echo server.$n=$WK:2888:3888 >> /tmp/zk_conf/zoo.cfg"
	n=$(($n+1))
done 10< $WORKERS

# Start MASTER
echo "-- Starting Zookeeper in MASTER $MASTER"
ssh $MASTER "source /etc/profile; module load singularity; singularity exec instance://spark zkServer.sh start"

# Set Workers
n=1
while read -u10 WK; do
        n=$(($n+1))
	echo "-- Configuring Zookeeper in WORKER $WK"	

	# Making directories and copying configuration files
	ssh $WK "rm -r /tmp/zookeeper /tmp/zk_logs; mkdir -p /tmp/zookeeper /tmp/zk_logs"
	ssh $WK cp $HOME/via-riscv/singularity/build/confcluster/conf_zk/conf/* /tmp/zk_conf/
	echo $n > /tmp/zookeeper/myid_sample
        scp /tmp/zookeeper/myid_sample $WK:/tmp/zookeeper/myid

	# Edit configuration
        ssh $WK echo "server.1=$MASTER:2888:3888 >> /tmp/zk_conf/zoo.cfg"
	m=2
        while read -u10 WK_SERVER; do
	     if [[ $m -eq $n ]]
	     then
		     ssh $WK "echo server.$m=0.0.0.0:2888:3888 >> /tmp/zk_conf/zoo.cfg"
	     else
                     ssh $WK "echo server.$m=$WK_SERVER:2888:3888 >> /tmp/zk_conf/zoo.cfg"
	     fi
	     m=$(($m+1))	 
     	done 10< $WORKERS

	echo "-- Starting Zookeeper in WORKER $WK"
        ssh $WK "source /etc/profile; module load singularity; singularity exec instance://spark zkServer.sh start"
done 10< $WORKERS

# View Zookeeper status
echo "Checking Zookeeper status in MASTER $MASTER"
ssh $MASTER "source /etc/profile; module load singularity; singularity exec instance://spark zkServer.sh status"
while read -u10 WK; do
	echo "Checking Zookeeper status in WORKER $WORKER"
	ssh $WK "source /etc/profile; module load singularity; singularity exec instance://spark zkServer.sh status"
done 10< $WORKERS

