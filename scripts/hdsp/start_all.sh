# Read workers and master file
# Pease make sure you have ssh connection to all the workers 
# Please make sure that the all the workers have the spark.sif in a folder named spark in their root user directory or change the path in the code below

# Set Master
MASTER=$(cat $HOME/via-riscv/scripts/hdsp/master.txt)
WORKERS=$HOME/via-riscv/scripts/hdsp/workers.txt
IMAGE_PATH=$HOME/via-riscv/singularity/iriscv_hbsp_mdr.sif
CORES=2

echo -e "\n------------------"
echo "Running START_ALL.py"
echo -e "------------------\n"

echo "-- Creating singularity instances in workers"
# Create spark instances
while read -u10 WORKER; do 
	echo "-- Creating tmp directories and starting singularity instance in WORKER $WORKER"
	ssh $WORKER rm -r /tmp/hdfs /tmp/spark /tmp/zk_conf
        ssh $WORKER mkdir -p /tmp/hdfs/namenode /tmp/hdfs/datanode
	ssh $WORKER mkdir -p /tmp/spark/logs /tmp/spark/work
	ssh $WORKER mkdir -p /tmp/zk_conf
        ssh $WORKER "source /etc/profile; module load singularity; singularity instance start --bind /tmp/spark/logs/:/opt/spark/logs,/tmp/spark/work/:/opt/spark/work,/tmp/zk_conf/:/usr/local/zookeeper/conf $IMAGE_PATH spark"
done 10< $WORKERS

echo "-- Creating tmp directories and starting singularity instance in MASTER $MASTER"
ssh $MASTER rm -r /tmp/hdfs /tmp/spark /tmp/zk_conf
ssh $MASTER mkdir -p /tmp/hdfs/namenode /tmp/hdfs/datanode
ssh $MASTER mkdir -p /tmp/spark/logs /tmp/spark/work
ssh $MASTER mkdir -p /tmp/zk_conf
ssh $MASTER "source /etc/profile; module load singularity; singularity instance start --bind /tmp/spark/logs/:/opt/spark/logs,/tmp/spark/work/:/opt/spark/work,/tmp/zk_conf/:/usr/local/zookeeper/conf $IMAGE_PATH spark"

# Init Zookeeper
echo "-- Starting ZOOKEEPER"
./zookeeper_start.sh master.txt workers.txt

# Init spark and hdfs in master
echo "-- Starting SPARK and HDFS in MASTER $MASTER with $CORES cores."
ssh $MASTER "source /etc/profile; module load singularity; singularity exec instance://spark spark-daemon.sh start org.apache.spark.deploy.master.Master 1 --host $MASTER --port 7078 --webui-port 8081"
ssh $MASTER echo 'Y' | singularity exec instance://spark hdfs namenode -format
ssh $MASTER "source /etc/profile; module load singularity; singularity exec instance://spark hdfs --daemon start namenode"
ssh $MASTER "source /etc/profile; module load singularity; singularity exec instance://spark yarn --daemon start resourcemanager"

# Init spark and hdfs in workers
n=1
while read -u10 WORKER; do 
        port=$((8081+$n))
	echo "-- Starting SPARK and HDFS in WORKER $WORKER with $CORES cores."
	ssh $WORKER "source /etc/profile; module load singularity; singularity exec instance://spark spark-daemon.sh start org.apache.spark.deploy.worker.Worker $n $MASTER:7078 --webui-port $port --cores $CORES;"
        ssh $WORKER "source /etc/profile; module load singularity; singularity exec instance://spark hdfs --daemon start datanode"
        ssh $WORKER "source /etc/profile; module load singularity; singularity exec instance://spark yarn --daemon start nodemanager"
	n=$(($n+1))
done 10< $WORKERS


echo ""
echo -e "\n------------------"
echo "START_ALL.py done!"
echo -e "------------------\n"


